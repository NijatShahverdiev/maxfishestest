
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Cell {

    public static void main(String[] args) {
        System.out.println(maxBinaryGap(328));
    }

    static int maxBinaryGap(int n){
        String binary = Integer.toBinaryString(n);
        System.out.println(binary);
        List<Integer> zeros = new ArrayList<>();
        int countZeros = 0;
        for (int i=1; i< binary.length(); i++){
            if(binary.charAt(i) =='0'){
                countZeros +=1;
            }else {
                zeros.add(countZeros);
                countZeros = 0;
            }
        }
        if (binary.charAt(binary.length()-1) == '0' && zeros.size() < 2)
            return 0;
        return Collections.max(zeros);
    }
}
