import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FishesInTheSea {

    public int solve(int[][] matrix) {
        List<Integer> resultList = new ArrayList<>();
        for (int row = 0; row < matrix.length; row++) {
            calculate(matrix, resultList, row, 0, matrix[row][0]);
        }
        return Collections.max(resultList);
    }

    public static void calculate(int[][] matrix, List<Integer> resultList, int row, int column, int startPoint) {
        int checkPoint = 0;
        for (int i = -1; i < 2; i++) {
            try {
                int currentSum = startPoint + matrix[row + i][column + 1];
                calculate(matrix, resultList, row + i, column + 1, currentSum);
                checkPoint++;
            } catch (Exception ignored) {
            }
        }
        if (checkPoint == 0) {
            resultList.add(startPoint);
        }
    }
}
