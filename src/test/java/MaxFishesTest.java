import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MaxFishesTest {

    FishesInTheSea fishes = new FishesInTheSea();
    final int[][] matrix1 = {
            {1, 3, 1, 5},
            {2, 2, 4, 1},
            {5, 0, 2, 3},
            {0, 6, 1, 2}
    };
    final int[][] matrix2 = {
            {10, 33, 13, 15},
            {22, 21, 4, 1},
            {5, 0, 2, 3},
            {0, 6, 14, 2}
    };
    final int[][] matrix3 = {
            {36, 5, 46, 29, 13, 57, 24, 95},
            {82, 45, 14, 67, 34, 64, 43, 50},
            {87, 8, 76, 78, 88, 84, 3, 41},
            {54, 99, 32, 60, 76, 68, 39, 12},
            {26, 86, 94, 39, 95, 70, 34, 78}
    };

    @Test
    void givenArrayIs3x3() {
        Assertions.assertEquals(16, fishes.solve(matrix1));
    }

    @Test
    void givenArrayIs4x4() {
        Assertions.assertEquals(83, fishes.solve(matrix2));
    }

    @Test
    void givenArrayIsNotSquareMatrix() {
        Assertions.assertEquals(650, fishes.solve(matrix3));
    }

}
